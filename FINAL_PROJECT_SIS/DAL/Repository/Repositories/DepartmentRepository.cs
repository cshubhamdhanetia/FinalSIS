﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.DATA;

namespace DAL.Repository.Repositories
{
    public class DepartmentRepository<TEntity> : GenericRepository<TEntity>, IDepartmentRepository where TEntity : class
    {
        public DepartmentRepository(StudentInformationSystemEntities context)
            : base(context)
        {
        }
    }
}

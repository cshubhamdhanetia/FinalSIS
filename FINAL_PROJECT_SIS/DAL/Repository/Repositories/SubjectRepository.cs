﻿using DAL.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository.Repositories
{
    public class SubjectRepository<TEntity> : GenericRepository<TEntity>, ISubjectRepository where TEntity : class
    {
        public SubjectRepository(StudentInformationSystemEntities context)
            : base(context)
        {
        }

    }
}

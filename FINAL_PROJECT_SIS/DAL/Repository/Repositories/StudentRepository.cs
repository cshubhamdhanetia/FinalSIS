﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.DATA;

namespace DAL.Repository.Repositories
{
    public class StudentRepository<TEntity> : GenericRepository<TEntity>, IStudentRepository where TEntity : class
    {
        public StudentRepository(StudentInformationSystemEntities context)
            : base(context)
        {
        }

    }
}

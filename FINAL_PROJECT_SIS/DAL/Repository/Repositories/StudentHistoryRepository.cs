﻿using DAL.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository.Repositories
{
    public class StudentHistoryRepository<TEntity> : GenericRepository<TEntity>, IStudentHistoryRepository where TEntity : class
    {
        public StudentHistoryRepository(StudentInformationSystemEntities context)
            : base(context)
        {
        }

    }
}

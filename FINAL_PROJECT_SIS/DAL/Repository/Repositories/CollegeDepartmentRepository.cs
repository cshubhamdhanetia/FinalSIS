﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.DATA;

namespace DAL.Repository.Repositories
{
    public class CollegeDepartmentRepository<TEntity> : GenericRepository<TEntity>, ICollegeDepartmentRepository where TEntity : class
    {
        public CollegeDepartmentRepository(StudentInformationSystemEntities context)
            : base(context)
        {
        }

    }
}

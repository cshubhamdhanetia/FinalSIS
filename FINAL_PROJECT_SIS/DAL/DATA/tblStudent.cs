//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.DATA
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblStudent
    {
        public tblStudent()
        {
            this.tblStudentHistories = new HashSet<tblStudentHistory>();
        }
    
        public System.Guid student_id { get; set; }
        public string student_email { get; set; }
        public System.DateTime student_dateofbirth { get; set; }
        public string student_gender { get; set; }
        public string student_phone { get; set; }
        public string student_firstname { get; set; }
        public string student_middlename { get; set; }
        public string student_lastname { get; set; }
        public System.DateTime student_dateofjoining { get; set; }
        public string student_address { get; set; }
        public Nullable<int> city_id { get; set; }
        public Nullable<int> state_id { get; set; }
    
        public virtual tblCity tblCity { get; set; }
        public virtual tblState tblState { get; set; }
        public virtual ICollection<tblStudentHistory> tblStudentHistories { get; set; }
    }
}

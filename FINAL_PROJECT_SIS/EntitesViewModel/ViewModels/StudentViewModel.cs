﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EntitesViewModel.ViewModels
{
    public class StudentViewModel
    {
        //[ScaffoldColumn(false)]
        public Guid student_id { get; set; }

        //[Display(Name="Email id")]
        //[Required(ErrorMessage="Email id is required")]
        //[RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string email { get; set; }

        //[Display(Name="Date of Birth")]
        //[Required(ErrorMessage="Date of birth is required")]
        //[DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime dob { get; set; } // Date Of Birth

        //[Display(Name="Gender")]
        //[Required(ErrorMessage="Gender is required")]
        public string gender { get; set; }

        //[Display(Name = "Contact no")]
        //[Required(ErrorMessage = "Contact number is required")]
        ////  [DataType(DataType.PhoneNumber)]
        //[RegularExpression(@"^([0-9]{10})$", ErrorMessage = "Invalid Mobile Number.")]
        public string phone { get; set; }

        //[Display(Name = "First Name")]
        //[Required(ErrorMessage = "Firstname is required")]
        //[RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Only Alphabets are Allowed")]
        public string firstname { get; set; }

        //[Display(Name = "Middle Name")]
        //[Required(ErrorMessage = "Middlename is required")]
        //[RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Only Alphabets are Allowed")]
        public string middlename { get; set; }

        //[Display(Name = "Last Name")]
        //[Required(ErrorMessage = "Lastname is required")]
        //[RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Only Alphabets are Allowed")]
        public string lastname { get; set; }

        public string fullname { get; set; }

        //[Display(Name = "Name")]
        public string Name
        {
            get { return firstname + " " + middlename+" " +lastname; }
        }

        //[Display(Name = "Enrollment Number")]
        //[Required(ErrorMessage = "Enrollment Number is required")]
        //[RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Alphabets and Numbers allowed.")]
        //[StringLength(12, ErrorMessage = "Please enter valid Enrollment Number")]
        public DateTime enrollment_date { get; set; }

         //[Required(ErrorMessage = "Address is required")]
        public string address { get; set; }

        public CityViewModel Cityviewmodel { get; set; }
        public StateViewModel Stateviewmodel { get; set; }
        public List<StudentHistoryViewModel> StudentHistoryModelList { get; set; }
        public CollegeViewModel CollegeViewModel { get; set; }
        public RoleViewModel RoleViewModel { get; set; }
        public DepartmentViewModel DepartmentViewModel { get; set; }
        public SemesterViewModel SemesterViewModel { get; set; }

        public StudentViewModel()
        {
            Cityviewmodel = new CityViewModel();
            Stateviewmodel = new StateViewModel();
            StudentHistoryModelList = new List<StudentHistoryViewModel>();
            CollegeViewModel = new CollegeViewModel();
            RoleViewModel = new RoleViewModel();
            DepartmentViewModel = new DepartmentViewModel();
            SemesterViewModel = new SemesterViewModel();

        }

    }
}

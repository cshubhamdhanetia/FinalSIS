﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EntitesViewModel.ViewModels
{
    public class SubjectViewModel
    {
        [ScaffoldColumn(false)]
        public int SubjectId { get; set; }

        
        public string SubjectCode { get; set; }

        [Display(Name="Subject name")]
        [Required]
        [DataType(DataType.Text)]
        public string SubjectName { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EntitesViewModel.ViewModels
{
    public class SemesterViewModel
    {
        [ScaffoldColumn(false)]
        public int sem_id { get; set; }

        [Display(Name = "Semester No")]
        [Required(ErrorMessage = "Semester No is required")]
        public int sem_no { get; set; }
    }
}

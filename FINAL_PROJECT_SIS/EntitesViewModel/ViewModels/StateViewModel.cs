﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EntitesViewModel.ViewModels
{
    public class StateViewModel
    {
        [ScaffoldColumn(false)]
        public int state_id { get; set; }

        [Display(Name="State Name")]
        [Required(ErrorMessage="State name is required")]
        public string state_name { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EntitesViewModel.ViewModels
{
    public class DepartmentViewModel
    {
        [ScaffoldColumn(false)]
        public int department_id { get; set; }

        [Display(Name = "Daprtment name")]
        [Required(ErrorMessage = "Department name is required")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Only Alphabets are Allowed")]
        public string department_name { get; set; }
        public string department_code { get; set; }
        

        public List<SubjectViewModel> subjectviewmodellist { get; set; }

        public DepartmentViewModel()
        {
            subjectviewmodellist = new List<SubjectViewModel>();
        }

    }
}

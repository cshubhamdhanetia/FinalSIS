﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace EntitesViewModel.ViewModels
{
    public class LoginViewModel
    {
        //[Display(Name = "Email ID")]
        //[Required(ErrorMessage = "Email ID is required")]
        //// [DataType(DataType.EmailAddress)]
        //[RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string email { get; set; }

        //[Display(Name = "Password")]
        //[Required(ErrorMessage = "password is required")]
        //[DataType(DataType.Password)]
        //[StringLength(18, MinimumLength = 6, ErrorMessage = "Length Of Password should be between 6 and 18")]
        public string password { get; set; }
    }
}

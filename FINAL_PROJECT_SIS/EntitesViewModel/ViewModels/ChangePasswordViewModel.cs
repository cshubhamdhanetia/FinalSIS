﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EntitesViewModel.ViewModels
{
    public class ChangePasswordViewModel
    {
        [Display(Name = "Email ID")]
        [Required(ErrorMessage = "Email ID is required")]
        // [DataType(DataType.EmailAddress)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public String email { get; set; }


        [Display(Name = "Password")]
        [Required(ErrorMessage = "password is required")]
        [StringLength(18, MinimumLength = 6, ErrorMessage = "Length Of Password should be between 6 and 18")]
        [DataType(DataType.Password)]
        public String password { get; set; }

        [Display(Name = "New Password")]
        [Required(ErrorMessage = "New Password is required")]
        [StringLength(18, MinimumLength = 6, ErrorMessage = "Length Of Password should be between 6 and 18")]
        [DataType(DataType.Password)]
        public String newpassword { get; set; }

        [Display(Name = "Confirm Password")]
        [Required(ErrorMessage = "Confirm Password is required")]
        [DataType(DataType.Password)]
        [Compare("newpassword")]
        [StringLength(18, MinimumLength = 6, ErrorMessage = "Length Of Password should be between 6 and 18")]
        public String newpassword1 { get; set; } // For Re-Entered Password
    }
}


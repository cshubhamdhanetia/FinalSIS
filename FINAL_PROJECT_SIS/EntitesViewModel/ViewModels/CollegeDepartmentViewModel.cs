﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitesViewModel.ViewModels
{
    public class CollegeDepartmentViewModel
    {
        public Guid id { get; set; }
        public CollegeViewModel collegeviewmodel { get; set; }
        public List<DepartmentViewModel> departmentviewmodel { get; set; }

        public CollegeDepartmentViewModel()
        {
            collegeviewmodel = new CollegeViewModel();
            departmentviewmodel = new List<DepartmentViewModel>();
        }


    }
}

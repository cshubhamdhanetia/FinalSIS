﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitesViewModel.ViewModels
{
    public class CityViewModel
    {
        [ScaffoldColumn(false)]
        public int city_id { get; set; }

        [Display(Name = "City name")]
        [Required(ErrorMessage = "City name is required")]
        public string city_name { get; set; }


    }
}

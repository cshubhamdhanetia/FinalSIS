﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitesViewModel.ViewModels
{
    public class CollegeDepartmentHelperPortal
    {
        public CollegeViewModel collegeViewModel { get; set; }
        public DepartmentViewModel departmentViewModel { get; set; }

        public CollegeDepartmentHelperPortal()
        {
            collegeViewModel = new CollegeViewModel();
            departmentViewModel = new DepartmentViewModel();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EntitesViewModel.ViewModels
{
    public class CollegeViewModel
    {
        [ScaffoldColumn(false)]
        public int college_id { get; set; }

        [Display(Name = "College Name")]
        [Required(ErrorMessage = "College name is required")]
        public string college_name { get; set; }

        [Display(Name = "Address of College")]
        [Required(ErrorMessage = "College's Address is required")]
        public string address { get; set; }

        [Display(Name = "Contact no of College")]
        [Required(ErrorMessage = "Contact number is required")]
      //  [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^([0-9]{10})$", ErrorMessage = "Invalid Mobile Number.")]
        public string contactno { get; set; }

        public CityViewModel citymodel { get; set; }
        public UniversityViewModel universitymodel { get; set; }
        public StateViewModel stateviewmodel { get; set; }
        public List<DepartmentViewModel> departmentlist { get; set; }
        public CollegeViewModel()
        {
            citymodel = new CityViewModel();
            universitymodel = new UniversityViewModel();
            stateviewmodel = new StateViewModel();
            departmentlist = new List<DepartmentViewModel>();

        }





    }
}

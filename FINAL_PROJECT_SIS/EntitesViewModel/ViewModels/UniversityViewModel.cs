﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EntitesViewModel.ViewModels
{
    public class UniversityViewModel
    {
        [ScaffoldColumn(false)]
        public int UniversityId { get; set; }

        [Display(Name="University name")]
        [Required]
        public string UniversityName { get; set; }
        public List<CollegeViewModel> collegelist { get; set; }
        public string founder { get; set; }
        public int number_of_students { get; set; }
        public UniversityViewModel()
        {
            collegelist = new List<CollegeViewModel>();
        }

    }
}

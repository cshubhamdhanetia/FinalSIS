﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitesViewModel.ViewModels
{
    public class StudentHistoryViewModel
    {
        [ScaffoldColumn(false)]
        public Guid id { get; set; }

        [ScaffoldColumn(false)]
        public DateTime LogDate { get; set; }

        public SemesterViewModel SemesterViewModel { get; set; }
        public DepartmentViewModel DepartmentViewModel { get; set; }
        public CollegeViewModel CollegeViewModel { get; set; }

        public StudentHistoryViewModel()
        {
            SemesterViewModel = new SemesterViewModel();
            DepartmentViewModel = new DepartmentViewModel();
            CollegeViewModel = new CollegeViewModel();
        }


    }
}

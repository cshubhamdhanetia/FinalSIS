﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitesViewModel.ViewModels
{
    public class RoleViewModel
    {
        [ScaffoldColumn(false)]
        public int role_id { get; set; }

        [Required]
        public string role_name { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitesViewModel.ViewModels;

namespace BusinessLayer
{
    public class UniversityService
    {
        public UniversityViewModel GetUniversityDetails()
        {
            MasterRepository masterrepo = new MasterRepository();
            UniversityViewModel universityviewmodel = new UniversityViewModel();
            var master = masterrepo.UniversityRepository.Get().FirstOrDefault();
            if (master != null)
            {
                universityviewmodel.UniversityId = master.university_id;
                universityviewmodel.UniversityName = master.university_name;
                universityviewmodel.founder = master.founder_name;
                universityviewmodel.collegelist = Service.CollegeService.GetCollegeListByUniversityId(master.university_id);
                universityviewmodel.number_of_students = masterrepo.StudentRepository.Get().Count();
                return universityviewmodel;
            }

            return null;
        }

        public List<UniversityViewModel> GetUniversityList()
        {
            List<UniversityViewModel> UniversityList = new List<UniversityViewModel>();
            MasterRepository masterRepository = new MasterRepository();

            var universityobj = masterRepository.UniversityRepository.Get().ToList();

            foreach (var university in universityobj)
            {
                UniversityViewModel universitymodel = new UniversityViewModel();
                universitymodel.UniversityId = university.university_id;
                universitymodel.UniversityName = university.university_name;

                UniversityList.Add(universitymodel);
            }
            return UniversityList;

        }
        
    }
}

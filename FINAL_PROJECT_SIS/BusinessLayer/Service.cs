﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public static class Service
    {
        public static CollegeService CollegeService { get; set; }
        public static DataService DataService { get; set; }
        public static DepartmentService DepartmentService { get; set; }
        public static AuthenticationService AuthenticationService { get; set; }
        public static RoleService RoleService { get; set; }
        public static StudentService StudentService { get; set; }
        public static UniversityService UniversityService { get; set; }
        public static EncryptionService EncryptionService { get; set; }


        static Service()
        {
            if(CollegeService==null)
            {
                CollegeService = new CollegeService();
            }
            if (DepartmentService == null)
            {
                DepartmentService = new DepartmentService();
            }
            if (AuthenticationService == null)
            {
                AuthenticationService = new AuthenticationService();
            }
            if (UniversityService == null)
            {
                UniversityService = new UniversityService();
            }
            if (StudentService == null)
            {
                StudentService = new StudentService();
            }
            if (RoleService == null)
            {
                RoleService = new RoleService();
            }
            if (DataService == null)
            {
                DataService = new DataService();
            }
            if (EncryptionService == null)
            {
                EncryptionService = new EncryptionService();
            }   
                    
            
           
            
           

        }
    }
}

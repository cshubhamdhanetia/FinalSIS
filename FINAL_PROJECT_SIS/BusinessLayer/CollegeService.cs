﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitesViewModel.ViewModels;
using DAL.DATA;

namespace BusinessLayer
{
    public class CollegeService
    {



        public List<CollegeViewModel> GetCollegeList()
        {
            MasterRepository masterRepository = new MasterRepository();

            List<CollegeViewModel> CollegeList = new List<CollegeViewModel>();

            var collegelist = masterRepository.CollegeRepository.Get().ToList();

            foreach (var college in collegelist)
            {
                CollegeViewModel collegemodel = new CollegeViewModel();

                collegemodel.address = college.address;
                collegemodel.citymodel = Service.DataService.GetCityById(college.city_id);
                collegemodel.college_id = college.college_id;
                collegemodel.college_name = college.college_name;
                collegemodel.contactno = college.contact_no;
                collegemodel.departmentlist = Service.DepartmentService.GetDepartmentList();
                collegemodel.stateviewmodel = Service.DataService.GetStateById((int)college.state_id);





                CollegeList.Add(collegemodel);
            }
            return CollegeList;

        }

        public CollegeViewModel GetCollegeByID(int id)
        {

            CollegeViewModel collegemodel = new CollegeViewModel();

            var college = new MasterRepository().CollegeRepository.Get(filter: p => p.college_id == id).FirstOrDefault();

            collegemodel.address = college.address;
            collegemodel.citymodel = Service.DataService.GetCityById(college.city_id);
            collegemodel.college_id = college.college_id;
            collegemodel.college_name = college.college_name;
            collegemodel.contactno = college.contact_no;
            //collegemodel.departmentlist = Service.DepartmentService.GetDepartmentList();
            collegemodel.departmentlist = Service.DepartmentService.GetDepartmentByCollegeId(college.college_id);


            return collegemodel;

        }
        public bool DeleteDepartment(int college_id, int department_id)
        {
            MasterRepository masterRepository = new MasterRepository();
            var obj = masterRepository.CollegeDepartmentRepository.Get(filter: p => p.college_id == college_id && p.department_id == department_id).FirstOrDefault();
            if (obj != null)
            {
                masterRepository.CollegeDepartmentRepository.Delete(obj);
                masterRepository.Save();
                return true;
            }
            return false;
        }
        public bool AddDepartment(CollegeDepartmentHelperPortal collegeDepartmentHelperPortal)
        {
            try
            {

                MasterRepository masterRepository = new MasterRepository();
                var CheckIfDepartmentExists = masterRepository.CollegeDepartmentRepository.Get(filter: p => p.department_id == collegeDepartmentHelperPortal.departmentViewModel.department_id && p.college_id == collegeDepartmentHelperPortal.collegeViewModel.college_id).SingleOrDefault();
                if (CheckIfDepartmentExists == null)
                {
                    tblCollegeDepartment tblCollege = new tblCollegeDepartment();
                    tblCollege.college_id = collegeDepartmentHelperPortal.collegeViewModel.college_id;
                    tblCollege.department_id = collegeDepartmentHelperPortal.departmentViewModel.department_id;
                    tblCollege.id = Guid.NewGuid();
                    masterRepository.CollegeDepartmentRepository.Insert(tblCollege);
                    masterRepository.Save();
                    return true;
                }
                return false;

            }
            catch
            {
                return false;
            }


        }

        public List<CollegeViewModel> GetCollegeListByUniversityId(int id)
        {
            MasterRepository masterRepository = new MasterRepository();

            List<CollegeViewModel> CollegeList = new List<CollegeViewModel>();

            var collegelist = masterRepository.CollegeRepository.Get(filter:prop=>prop.university_id==id).ToList();

            foreach (var college in collegelist)
            {
                CollegeViewModel collegemodel = new CollegeViewModel();

                collegemodel.address = college.address;
                collegemodel.citymodel = Service.DataService.GetCityById(college.city_id);
                collegemodel.college_id = college.college_id;
                collegemodel.college_name = college.college_name;
                collegemodel.contactno = college.contact_no;
                collegemodel.departmentlist = Service.DepartmentService.GetDepartmentList();
                collegemodel.stateviewmodel = Service.DataService.GetStateById((int)college.state_id);
                CollegeList.Add(collegemodel);
            }
            return CollegeList;

        }
        public bool UpdateCollege(CollegeViewModel collegeviewmodel)
        {
            try {
                MasterRepository masterrepo = new MasterRepository();
                tblCollege tblcollege = new tblCollege();
                tblcollege = masterrepo.CollegeRepository.Get(p => p.college_id == collegeviewmodel.college_id).FirstOrDefault();
                if (tblcollege != null)
                {
                    tblcollege.address = collegeviewmodel.address;
                    tblcollege.city_id = collegeviewmodel.citymodel.city_id;
                    tblcollege.college_name = collegeviewmodel.college_name;
                    tblcollege.contact_no = collegeviewmodel.contactno;
                    tblcollege.state_id = collegeviewmodel.stateviewmodel.state_id;
                    tblcollege.university_id = collegeviewmodel.universitymodel.UniversityId;
                    masterrepo.CollegeRepository.Update(tblcollege);
                    masterrepo.Save();


                }
 
                return true;
            
            }
            catch (Exception e) { return false; }
            
          
        }

        public bool DeleteCollege(int id)
        {
            MasterRepository master = new MasterRepository();
            try 
            {
                var clgobj = master.CollegeRepository.Get(p=>p.college_id==id).First();
                var collegedeplist = master.CollegeDepartmentRepository.Get(filter: prop => prop.college_id == id).ToList();
                var studenthistory = master.StudentHistoryRepository.Get(filter: prop => prop.college_id == id).ToList();

                foreach (var collegedep in collegedeplist)
                {
                    master.CollegeDepartmentRepository.Delete(collegedep);
                }
                foreach (var studenthistoryobj in studenthistory)
                {
                    master.StudentHistoryRepository.Delete(studenthistoryobj);
                }
                master.CollegeRepository.Delete(clgobj);
                master.Save();
                return true;
            }
            catch
            {
                return false;
            }       


           
        }


        public bool AddCollege(CollegeViewModel collegeviewmodel) {
            try
            {
                MasterRepository masterrepo = new MasterRepository();
                tblCollege clg = new tblCollege();
                clg.address = collegeviewmodel.address;
                clg.city_id = collegeviewmodel.citymodel.city_id;
                clg.college_id = collegeviewmodel.college_id;
                clg.college_name = collegeviewmodel.college_name;
                clg.contact_no = collegeviewmodel.contactno;
                clg.state_id = collegeviewmodel.stateviewmodel.state_id;
                clg.university_id = collegeviewmodel.universitymodel.UniversityId;
                masterrepo.CollegeRepository.Insert(clg);
                masterrepo.Save();
                return true;

            }
            catch (Exception e)
            {
                return false;

            }        


        }
            




    }
}

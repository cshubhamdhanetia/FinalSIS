﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitesViewModel.ViewModels;

namespace BusinessLayer
{
    public class RoleService 
    {
        
        public List<RoleViewModel> GetRoleList()
        {
   
            List<RoleViewModel> RoleList = new List<RoleViewModel>();

            var roleobj = new MasterRepository().RolesRepository.Get().ToList();

            foreach (var role in roleobj)
            {
                RoleViewModel roleviewmodel = new RoleViewModel();
                roleviewmodel.role_id = role.role_id;
                roleviewmodel.role_name = role.role_name;


                RoleList.Add(roleviewmodel);
            }
            return RoleList;

        }

        public RoleViewModel GetRoleById(int id)
        {
            RoleViewModel rolemodel = new RoleViewModel();

            var roleobj = new MasterRepository().RolesRepository.Get(filter: p => p.role_id == id).FirstOrDefault();

            rolemodel.role_id = roleobj.role_id;
            rolemodel.role_name = roleobj.role_name;

            return rolemodel;

        }

    }
}

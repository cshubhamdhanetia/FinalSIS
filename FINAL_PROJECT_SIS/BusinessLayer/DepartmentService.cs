﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitesViewModel.ViewModels;
using DAL.DATA;

namespace BusinessLayer
{
    public class DepartmentService
    {

        public List<DepartmentViewModel> GetDepartmentList()
        {
            List<DepartmentViewModel> DepartmentList = new List<DepartmentViewModel>();

            var departmentlist = new MasterRepository().DepartmentRepository.Get().ToList();

            foreach (var department in departmentlist)
            {
                DepartmentViewModel departmentviewmodel = new DepartmentViewModel();
                departmentviewmodel.department_id = department.department_id;
                departmentviewmodel.department_name = department.department_name;
                departmentviewmodel.department_code = department.department_code;
                DepartmentList.Add(departmentviewmodel);
            }
            return DepartmentList;

        }

        public DepartmentViewModel GetDepartmentById(int id)
        {
            DepartmentViewModel departmentmodel = new DepartmentViewModel();

            var department = new MasterRepository().DepartmentRepository.Get(filter: p => p.department_id == id).FirstOrDefault();

            departmentmodel.department_id = department.department_id;
            departmentmodel.department_name = department.department_name;
            departmentmodel.department_code = department.department_code;

            return departmentmodel;

        }

        public List<DepartmentViewModel> GetDepartmentByCollegeId(int id)
        {
            List<DepartmentViewModel> DepartmentList = new List<DepartmentViewModel>();

            var departmentlist = new MasterRepository().CollegeDepartmentRepository.Get(filter: p => p.college_id == id).ToList();

            foreach (var department in departmentlist)
            {
                DepartmentViewModel departmentviewmodel = new DepartmentViewModel();
                departmentviewmodel.department_id = (int)department.department_id;
                departmentviewmodel.department_name = (string)department.tblDepartment.department_name;
                departmentviewmodel.department_code = department.tblDepartment.department_code;


                DepartmentList.Add(departmentviewmodel);
            }
            return DepartmentList;

        }

        public bool AddDepartment(DepartmentViewModel departmentviewmodel)
        {
            try
            {
                MasterRepository master = new MasterRepository();
                tblDepartment dep = new tblDepartment();
                dep.department_code = departmentviewmodel.department_code;
                dep.department_name = departmentviewmodel.department_name;
                master.DepartmentRepository.Insert(dep);
                master.Save();               
                return true;
            }               
            catch (Exception e)
            {
                return false;
            }

        }

        public bool UpdateDepartment(DepartmentViewModel departmentviewmodel)
        {
            try
            {
                MasterRepository masterrepo = new MasterRepository();
                tblDepartment tbldepartment = new tblDepartment();
                tbldepartment = masterrepo.DepartmentRepository.Get(p => p.department_id == departmentviewmodel.department_id).FirstOrDefault();
                if (tbldepartment != null)
                {

                    tbldepartment.department_code = departmentviewmodel.department_code;
                    tbldepartment.department_name = departmentviewmodel.department_name;
                    masterrepo.DepartmentRepository.Update(tbldepartment);
                    masterrepo.Save();


                }

                return true;

            }
            catch (Exception e) { return false; }


        }



        public bool DeleteDepartment(int id)
        {
            MasterRepository master = new MasterRepository();
            try
            {
                var depobj = master.DepartmentRepository.Get(p => p.department_id == id).First();
                var collegedeplist = master.CollegeDepartmentRepository.Get(filter: prop => prop.department_id == id).ToList();
                var studenthistory = master.StudentHistoryRepository.Get(filter: prop => prop.department_id == id).ToList();

                foreach (var collegedep in collegedeplist)
                {
                    master.CollegeDepartmentRepository.Delete(collegedep);
                }
                foreach (var studenthistoryobj in studenthistory)
                {
                    master.StudentHistoryRepository.Delete(studenthistoryobj);
                }
                master.DepartmentRepository.Delete(depobj);
                master.Save();
                return true;
            }
            catch
            {
                return false;
            }



        }

    }
}

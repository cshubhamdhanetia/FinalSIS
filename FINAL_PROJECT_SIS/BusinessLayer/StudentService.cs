﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitesViewModel.ViewModels;
using System.Security.Cryptography;
using DAL.DATA;

namespace BusinessLayer
{
    public class StudentService 
    {
        public StudentService():base()
        {

        }
    

        public bool AddStudent(StudentViewModel studentviewmodel)
        {
            bool result;
            try { 
            MasterRepository MasterRepository = new MasterRepository();
            tblStudent student = new tblStudent();
            tblUserInfo userinfo = new tblUserInfo();
            tblStudentHistory studenthistory = new tblStudentHistory();
            student.city_id = studentviewmodel.Cityviewmodel.city_id;
            student.state_id = studentviewmodel.Stateviewmodel.state_id;
            student.student_address = studentviewmodel.address;
            student.student_dateofbirth = studentviewmodel.dob;
            student.student_dateofjoining = studentviewmodel.enrollment_date;
            student.student_email = studentviewmodel.email;
            student.student_firstname = studentviewmodel.firstname;
            student.student_gender = studentviewmodel.gender;
            student.student_id = Guid.NewGuid();
            student.student_lastname = studentviewmodel.lastname;
            student.student_middlename = studentviewmodel.middlename;
            student.student_phone = studentviewmodel.phone;

            userinfo.id = Guid.NewGuid();
            userinfo.email = student.student_email;
            var temp_pass = student.student_dateofbirth.ToString("dd/MM/yyyy");
            MD5 md5Hash = MD5.Create();
            var hash_pass = Service.EncryptionService.GetMd5Hash(md5Hash,temp_pass);
            userinfo.password = hash_pass;
            userinfo.role_id = studentviewmodel.RoleViewModel.role_id;

            studenthistory.id = Guid.NewGuid();
            studenthistory.LogDate = DateTime.Now;
            studenthistory.sem_id = studentviewmodel.SemesterViewModel.sem_id;
            studenthistory.student_id = student.student_id;
            studenthistory.college_id = studentviewmodel.CollegeViewModel.college_id;
            studenthistory.department_id = studentviewmodel.DepartmentViewModel.department_id;
            
            
            MasterRepository.StudentRepository.Insert(student);
            MasterRepository.StudentHistoryRepository.Insert(studenthistory);
            MasterRepository.UserInfoRepository.Insert(userinfo);
            
            
            MasterRepository.Save();
            result = true;
            return result;


            }
            catch (Exception e)
            {
                result = false;
                return result;
            }         
                      
        }


        public bool UpdateStudent(StudentViewModel studentviewmodel)
        {
            MasterRepository MasterRepository = new MasterRepository();
            tblStudent tblstudent = new tblStudent();
            tblUserInfo userinfo = new tblUserInfo();
            tblStudentHistory tblstudenthistory = new tblStudentHistory();           

            tblstudent = MasterRepository.StudentRepository.Get(p => p.student_id == studentviewmodel.student_id).FirstOrDefault();
            tblstudenthistory = MasterRepository.StudentHistoryRepository.Get(filter: prop => prop.student_id == tblstudent.student_id).LastOrDefault();
            userinfo = MasterRepository.UserInfoRepository.Get(filter: prop => prop.email == tblstudent.student_email).FirstOrDefault();

            


            try
            {
                if (tblstudent != null && userinfo !=null)
                {
                    tblstudent.city_id = studentviewmodel.Cityviewmodel.city_id;
                    tblstudent.state_id = studentviewmodel.Stateviewmodel.state_id;
                    tblstudent.student_address = studentviewmodel.address;
                    tblstudent.student_dateofbirth = studentviewmodel.dob;
                    tblstudent.student_dateofjoining = studentviewmodel.enrollment_date;
                    tblstudent.student_firstname = studentviewmodel.firstname;
                    tblstudent.student_gender = studentviewmodel.gender;
                    tblstudent.student_lastname = studentviewmodel.lastname;
                    tblstudent.student_middlename = studentviewmodel.middlename;
                    tblstudent.student_phone = studentviewmodel.phone;
                    tblstudent.student_email = studentviewmodel.email;

                    userinfo.email = studentviewmodel.email;
                    userinfo.role_id = studentviewmodel.RoleViewModel.role_id;
                  

                    MasterRepository.StudentRepository.Update(tblstudent);
                    MasterRepository.UserInfoRepository.Update(userinfo);

                    if (tblstudenthistory != null)
                    {
                        if (tblstudenthistory.college_id != studentviewmodel.CollegeViewModel.college_id
                            || tblstudenthistory.department_id != studentviewmodel.DepartmentViewModel.department_id
                            || tblstudenthistory.sem_id != studentviewmodel.SemesterViewModel.sem_id)
                        {
                            tblStudentHistory studenttemp = new tblStudentHistory();
                            studenttemp.id = Guid.NewGuid();

                            studenttemp.college_id = studentviewmodel.CollegeViewModel.college_id;
                            studenttemp.department_id = studentviewmodel.DepartmentViewModel.department_id;
                            studenttemp.sem_id = studentviewmodel.SemesterViewModel.sem_id;
                            studenttemp.LogDate = DateTime.Now;
                            studenttemp.student_id = studentviewmodel.student_id;
                            MasterRepository.StudentHistoryRepository.Insert(studenttemp);
                           


                        }



                    }
                    MasterRepository.Save();
                    return true;
                }                
               
            }
            catch (Exception e)
            {
                return false;
 
            }

            return false;
   
       }
            
            
            
        
        public List<StudentViewModel> GetStudentList()
        {
            MasterRepository masterrepo = new MasterRepository();
            List<StudentViewModel> studentlist = new List<StudentViewModel>();
            var studentobjlist = masterrepo.StudentRepository.Get().ToList();
            foreach (var studentobj in studentobjlist)
            {
                StudentViewModel studentviewmodel = new StudentViewModel();

                studentviewmodel.address = studentobj.student_address;
                studentviewmodel.Cityviewmodel = Service.DataService.GetCityById((int)studentobj.city_id);
                var current_status = StudentCurrentStatus(studentobj.student_id);
                studentviewmodel.CollegeViewModel = Service.CollegeService.GetCollegeByID(current_status.CollegeViewModel.college_id);
                studentviewmodel.DepartmentViewModel = Service.DepartmentService.GetDepartmentById(current_status.DepartmentViewModel.department_id);
                studentviewmodel.dob = studentobj.student_dateofbirth;
                studentviewmodel.email = studentobj.student_email;
                studentviewmodel.enrollment_date = studentobj.student_dateofjoining;
                studentviewmodel.firstname = studentobj.student_firstname;
                studentviewmodel.middlename = studentobj.student_middlename;
                studentviewmodel.lastname = studentobj.student_lastname;
                studentviewmodel.gender = studentobj.student_gender;
                studentviewmodel.phone = studentobj.student_phone;
                studentviewmodel.Stateviewmodel = Service.DataService.GetStateById((int)studentobj.state_id);
                studentviewmodel.student_id = studentobj.student_id;
                studentviewmodel.SemesterViewModel = Service.DataService.GetSemesterById(current_status.SemesterViewModel.sem_id);

                studentlist.Add(studentviewmodel);

                

            }

            return studentlist;
        }
        public StudentViewModel GetStudentById(Guid id)
        {
            var studentobj = new MasterRepository().StudentRepository.Get(filter: p => p.student_id == id).FirstOrDefault();
            StudentViewModel studentviewmodel = new StudentViewModel();
            if (studentobj != null)
            {
                studentviewmodel.address = studentobj.student_address;
                studentviewmodel.Cityviewmodel = Service.DataService.GetCityById((int)studentobj.city_id);
                var current_status = StudentCurrentStatus(studentobj.student_id);
                studentviewmodel.CollegeViewModel = Service.CollegeService.GetCollegeByID(current_status.CollegeViewModel.college_id);
                studentviewmodel.DepartmentViewModel = Service.DepartmentService.GetDepartmentById(current_status.DepartmentViewModel.department_id);
                studentviewmodel.dob = studentobj.student_dateofbirth;
                studentviewmodel.email = studentobj.student_email;
                studentviewmodel.enrollment_date = studentobj.student_dateofjoining;
                studentviewmodel.firstname = studentobj.student_firstname;
                studentviewmodel.middlename = studentobj.student_middlename;
                studentviewmodel.lastname = studentobj.student_lastname;
                studentviewmodel.gender = studentobj.student_gender;
                studentviewmodel.phone = studentobj.student_phone;
                studentviewmodel.Stateviewmodel = Service.DataService.GetStateById((int)studentobj.state_id);
                studentviewmodel.student_id = studentobj.student_id;
                studentviewmodel.SemesterViewModel = Service.DataService.GetSemesterById(current_status.SemesterViewModel.sem_id);

                studentviewmodel.StudentHistoryModelList = GetStudentHistory(studentobj.student_id);
                return studentviewmodel;
            }
            return null;
        }
        public StudentViewModel GetStudentByEmail(String email)
        {

            var studentobj = new MasterRepository().StudentRepository.Get(filter: p => p.student_email == email).FirstOrDefault();
            StudentViewModel studentviewmodel = new StudentViewModel();
            if(studentobj!=null)
            {
                studentviewmodel.address = studentobj.student_address;
                studentviewmodel.Cityviewmodel = Service.DataService.GetCityById((int)studentobj.city_id);
                var current_status = StudentCurrentStatus(studentobj.student_id);
                studentviewmodel.CollegeViewModel = Service.CollegeService.GetCollegeByID(current_status.CollegeViewModel.college_id);
                studentviewmodel.DepartmentViewModel = Service.DepartmentService.GetDepartmentById(current_status.DepartmentViewModel.department_id);
                studentviewmodel.dob = studentobj.student_dateofbirth;
                studentviewmodel.email = studentobj.student_email;
                studentviewmodel.enrollment_date = studentobj.student_dateofjoining;
                studentviewmodel.firstname = studentobj.student_firstname;
                studentviewmodel.middlename = studentobj.student_middlename;
                studentviewmodel.lastname = studentobj.student_lastname;
                studentviewmodel.gender = studentobj.student_gender;
                studentviewmodel.phone = studentobj.student_phone;
                studentviewmodel.Stateviewmodel = Service.DataService.GetStateById((int)studentobj.state_id);
                studentviewmodel.student_id = studentobj.student_id;
                studentviewmodel.SemesterViewModel = Service.DataService.GetSemesterById(current_status.SemesterViewModel.sem_id);

                studentviewmodel.StudentHistoryModelList = GetStudentHistory(studentobj.student_id);
                return studentviewmodel;
            }
            return null;
            
        }
        public bool DeleteStudent(Guid id)
        {          
            try
            {
                MasterRepository masterrepo = new MasterRepository();
                var student=masterrepo.StudentRepository.Get(filter: prop => prop.student_id == id).First();
                if (masterrepo != null)
                {
                    var studenthistory = masterrepo.StudentHistoryRepository.Get(filter: p => p.student_id == id).ToList();
                foreach (var history in studenthistory)
                {
                    masterrepo.StudentHistoryRepository.Delete(history);

                }
                var userinfoobj = masterrepo.UserInfoRepository.Get(filter: prop => prop.email == student.student_email).First();
                if (userinfoobj != null)
                {
                    masterrepo.UserInfoRepository.Delete(userinfoobj);
                   
                }             
                masterrepo.Save();
                if (student != null)
                {
                    masterrepo.StudentRepository.Delete(student);
                }
                masterrepo.Save();
                return true;

                }



            }
            catch (Exception e)
            {
                return false;
            }
            return false;
            
        }

        public StudentHistoryViewModel StudentCurrentStatus(Guid id)
        {
            var current_status = new MasterRepository().StudentHistoryRepository.Get(filter: p => p.student_id == id).LastOrDefault();
            StudentHistoryViewModel studenthistoryviewmodel = new StudentHistoryViewModel();
            studenthistoryviewmodel.CollegeViewModel = Service.CollegeService.GetCollegeByID((int)current_status.college_id);
            studenthistoryviewmodel.DepartmentViewModel = Service.DepartmentService.GetDepartmentById((int)current_status.department_id);
            studenthistoryviewmodel.SemesterViewModel = Service.DataService.GetSemesterById((int)current_status.sem_id);
            studenthistoryviewmodel.LogDate = current_status.LogDate;
            studenthistoryviewmodel.id = current_status.id;


            return studenthistoryviewmodel;
        }

        public List<StudentHistoryViewModel> GetStudentHistory(Guid id)
        {
            var StudentHistoryList = new MasterRepository().StudentHistoryRepository.Get(filter: p => p.student_id == id).ToList();
            List<StudentHistoryViewModel> StudentHistoryViewModelList = new List<StudentHistoryViewModel>();

            foreach (var studenthistory in StudentHistoryList)
            {

                StudentHistoryViewModel studenthistoryviewmodel = new StudentHistoryViewModel();
                studenthistoryviewmodel.CollegeViewModel = Service.CollegeService.GetCollegeByID((int)studenthistory.college_id);
                studenthistoryviewmodel.DepartmentViewModel = Service.DepartmentService.GetDepartmentById((int)studenthistory.department_id);
                studenthistoryviewmodel.SemesterViewModel = Service.DataService.GetSemesterById((int)studenthistory.sem_id);
                studenthistoryviewmodel.LogDate = studenthistory.LogDate;
                studenthistoryviewmodel.id = studenthistory.id;


                StudentHistoryViewModelList.Add(studenthistoryviewmodel);



            }




            return StudentHistoryViewModelList;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitesViewModel.ViewModels;
using System.Security.Cryptography;
using DAL.DATA;

namespace BusinessLayer
{
    public class AuthenticationService
    {
        
       
        public bool CheckLogin(string email, string password)
        {
            bool result;
            MD5 md5Hash = MD5.Create();
            string hash = Service.EncryptionService.GetMd5Hash(md5Hash, password);
            var user = new MasterRepository().UserInfoRepository.Get(filter: p => p.email == email && p.password==hash).FirstOrDefault();
            if(user!=null)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }
        public bool UpdatePassword(ChangePasswordViewModel changepass)
        {
            var person = GetUserInfo(changepass.email);
            if(person!=null)
            {
                MD5 md5Hash = MD5.Create();
                string hash = Service.EncryptionService.GetMd5Hash(md5Hash, changepass.password);

                if (person.password == hash)
                {
                    if (changepass.newpassword == changepass.newpassword1)
                    {
                        MasterRepository masterRepository = new MasterRepository();
                        var userinfo = masterRepository.UserInfoRepository.Get(filter: p => p.email==changepass.email).First();
                      var hashnew = Service.EncryptionService.GetMd5Hash(md5Hash, changepass.newpassword);
                      userinfo.password = hashnew;
                        masterRepository.UserInfoRepository.Update(userinfo);
                        masterRepository.Save(); 


                    return true;
                    }
                }
            }
          
            return false;
        }

        public UserInfoViewModel GetUserInfo(string email)
        {
            UserInfoViewModel userInfoViewModel = new UserInfoViewModel();
            var obj = new MasterRepository().UserInfoRepository.Get(filter: p => p.email == email).First();
            if(obj!=null)
            {
                userInfoViewModel.id = obj.id;
                userInfoViewModel.email = obj.email;
                userInfoViewModel.password = obj.password;
                userInfoViewModel.RoleViewModel = Service.RoleService.GetRoleById((int)obj.role_id);
                return userInfoViewModel;
            }
            return null;
            
        }


    }
}

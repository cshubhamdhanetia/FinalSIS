﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.DATA;
using DAL.Repository;
using DAL.Repository.Repositories;


namespace BusinessLayer
{
    public class MasterRepository : IDisposable
    {

        public StudentInformationSystemEntities context = new StudentInformationSystemEntities();
        public StudentRepository<tblStudent> StudentRepository { get; set; }
        public CollegeDepartmentRepository<tblCollegeDepartment> CollegeDepartmentRepository { get; set; }
        public CollegeRepository<tblCollege> CollegeRepository { get; set; }
        public DepartmentSubjectRepository<tblDepartmentSubject> DepatmentSubjectRepository { get; set; }
        public RolesRepository<tblRole> RolesRepository { get; set; }
        public StudentHistoryRepository<tblStudentHistory> StudentHistoryRepository { get; set; }
        public SubjectRepository<tblSubject> SubjectRepository { get; set; }
        public UniversityRepository<tblUniversity> UniversityRepository { get; set; }
        public UserInfoRepository<tblUserInfo> UserInfoRepository { get; set; }
        public DepartmentRepository<tblDepartment> DepartmentRepository { get; set; }
        public GenericRepository<tblCity> CityRepository { get; set; }
        public GenericRepository<tblState> StateRepository { get; set; }
        public GenericRepository<tblSemester> SemesterRepository { get; set; }


        public MasterRepository()
        {
            if (StudentRepository == null)
            {
                StudentRepository = new StudentRepository<tblStudent>(context);
            }

            if (SemesterRepository == null)
            {
                SemesterRepository = new GenericRepository<tblSemester>(context);
            }

            if (CityRepository == null)
            {
                CityRepository = new GenericRepository<tblCity>(context);
            }

            if (StateRepository == null)
            {
                StateRepository = new GenericRepository<tblState>(context);
            }


            if (CollegeDepartmentRepository == null)
            {
                CollegeDepartmentRepository = new CollegeDepartmentRepository<tblCollegeDepartment>(context);
            }


            if (CollegeRepository == null)
            {
                CollegeRepository = new CollegeRepository<tblCollege>(context);
            }


            if (DepatmentSubjectRepository == null)
            {
                DepatmentSubjectRepository = new DepartmentSubjectRepository<tblDepartmentSubject>(context);
            }

            if (StudentHistoryRepository == null)
            {
                StudentHistoryRepository = new StudentHistoryRepository<tblStudentHistory>(context);
            }

            if (RolesRepository == null)
            {
                RolesRepository = new RolesRepository<tblRole>(context);
            }

            if (SubjectRepository == null)
            {
                SubjectRepository = new SubjectRepository<tblSubject>(context);
            }

            if (DepartmentRepository == null)
            {
                DepartmentRepository = new DepartmentRepository<tblDepartment>(context);
            }

            if (UniversityRepository == null)
            {
                UniversityRepository = new UniversityRepository<tblUniversity>(context);
            }

            if (UserInfoRepository == null)
            {
                UserInfoRepository = new UserInfoRepository<tblUserInfo>(context);
            }
        }


        public void Save()
        {
            context.SaveChanges();

        }



        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }


}

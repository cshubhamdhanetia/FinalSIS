﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitesViewModel.ViewModels;

namespace BusinessLayer
{
    public class DataService 
    {
        
        public List<CityViewModel> GetCityList()
        {
            List<CityViewModel> CityList = new List<CityViewModel>();
            MasterRepository masterRepository = new MasterRepository();

            var citylist = masterRepository.CityRepository.Get().ToList();

            foreach (var city in citylist)
            {
                CityViewModel citymodel = new CityViewModel();
                citymodel.city_id = city.city_id;
                citymodel.city_name = city.city_name;

                CityList.Add(citymodel);
            }
            return CityList;

        }

        public CityViewModel GetCityById(int id)
        {
            CityViewModel citymodel = new CityViewModel();

            var city = new MasterRepository().CityRepository.Get(filter: p => p.city_id == id).FirstOrDefault();

            citymodel.city_id = city.city_id;
            citymodel.city_name = city.city_name;

            return citymodel;

        }

        public List<StateViewModel> GetStateList()
        {
            List<StateViewModel> StateList = new List<StateViewModel>();

            var statelistorig = new MasterRepository().StateRepository.Get().ToList();

            foreach (var state in statelistorig)
            {
                StateViewModel statemodel = new StateViewModel();
                statemodel.state_id = state.state_id;
                statemodel.state_name = state.state_name;

                StateList.Add(statemodel);
            }
            return StateList;

        }

        public StateViewModel GetStateById(int id)
        {
            StateViewModel stateviewmodel = new StateViewModel();

            var state = new MasterRepository().StateRepository.Get(filter: p => p.state_id == id).FirstOrDefault();

            stateviewmodel.state_id = state.state_id;
            stateviewmodel.state_name = state.state_name;

            return stateviewmodel;

        }


        public List<SemesterViewModel> GetSemesterList()
        {
            List<SemesterViewModel> SemesterList = new List<SemesterViewModel>();

            var semobj = new MasterRepository().SemesterRepository.Get().ToList();


            foreach (var sem in semobj)
            {
                SemesterViewModel semesterviewmodel = new SemesterViewModel();
                semesterviewmodel.sem_id = sem.sem_Id;
                semesterviewmodel.sem_no = sem.sem_no;

                SemesterList.Add(semesterviewmodel);
            }
            return SemesterList;

        }

        public SemesterViewModel GetSemesterById(int id)
        {
            SemesterViewModel SemModel = new SemesterViewModel();

            var SemObj = new MasterRepository().SemesterRepository.Get(filter: p => p.sem_Id == id).FirstOrDefault();

            SemModel.sem_id = SemObj.sem_Id;
            SemModel.sem_no = SemObj.sem_no;

            return SemModel;

        }


    }
}

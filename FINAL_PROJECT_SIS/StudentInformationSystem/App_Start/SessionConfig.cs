﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StudentInformationSystem.App_Start
{
    public class SessionExpireAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext.Session == null || httpContext.Session["Role"] == null)
            {

                return false;
            }



            return base.AuthorizeCore(httpContext);
        }


    }
}
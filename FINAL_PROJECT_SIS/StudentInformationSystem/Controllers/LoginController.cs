﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using BusinessLayer;
using EntitesViewModel.ViewModels;

namespace StudentInformationSystem.Controllers
{
    public class LoginController : Controller
    {
        

        public ActionResult Login(string ReturnUrl)
        {
            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }
        [HttpPost]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            // Lets first check if the Model is valid or not
            if (ModelState.IsValid)
            {
                
                    string email = model.email;
                    string password = model.password;


                bool login_result = Service.AuthenticationService.CheckLogin(model.email, model.password);

                  
                    if (login_result == true)
                    {

                        FormsAuthentication.SetAuthCookie(model.email, false);

                    var person = Service.AuthenticationService.GetUserInfo(model.email);
                    Session["Email"] = person.email;
                    Session["Role"] = person.RoleViewModel.role_name;

                        var authTicket = new FormsAuthenticationTicket(1, person.email, DateTime.Now, DateTime.Now.AddMinutes(20), false, person.RoleViewModel.role_name);
                        string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                        var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                        HttpContext.Response.Cookies.Add(authCookie);
                        if (returnUrl != null)
                        {
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            if (person.RoleViewModel.role_name == "Student")
                            {
                                return RedirectToAction("Index", "Student");
                            }
                            else if (person.RoleViewModel.role_name == "Admin")
                            {
                                return RedirectToAction("Index", "Admin");
                            }



                        }
                    }                    
                
            }
            ViewBag.error = "Invalid Authentication";
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Login", "Login");

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EntitesViewModel.ViewModels;
using BusinessLayer;
using StudentInformationSystem.App_Start;

namespace StudentInformationSystem.Controllers
{
    [HandleError]
    [SessionExpire]
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {

        
        public ActionResult Index()
        {
            UniversityViewModel u = new UniversityViewModel();
            try
            {
                var university = Service.UniversityService.GetUniversityDetails();
                return View(university);
            }
            catch { return View(u); }
        }

        public ActionResult AddStudent()
        {
            if (ModelState.IsValid)
            {

                StudentViewModel studentviewmodel = new StudentViewModel();
                ViewBag.city_id = new SelectList(Service.DataService.GetCityList(), "city_id", "city_name");
                ViewBag.state_id = new SelectList(Service.DataService.GetStateList(), "state_id", "state_name");
                ViewBag.university_id = new SelectList(Service.UniversityService.GetUniversityList(), "UniversityId", "UniversityName");
                ViewBag.college_id = new SelectList(Service.CollegeService.GetCollegeList(), "college_id", "college_name");
                ViewBag.sem_id = new SelectList(Service.DataService.GetSemesterList(), "sem_id", "sem_no");
                ViewBag.department_id = new SelectList(Service.DepartmentService.GetDepartmentList(), "department_id", "department_name");
                ViewBag.role_id = new SelectList(Service.RoleService.GetRoleList(), "role_id", "role_name");
                return View(studentviewmodel);

            }
            return View();
        }
        [HttpPost]
        public String AddStudent(StudentViewModel studentModel)
        {
            bool result = Service.StudentService.AddStudent(studentModel);
            if (result == true)
            {
                return "User added Successfully";
            }
            else
            {
                return "Error while Adding Student";
            }
            
        }

        public ActionResult ViewStudents()
        {
            List<StudentViewModel> studentviewmodellist = new List<StudentViewModel>();
            try
            {
                var studentlist = Service.StudentService.GetStudentList();
                return View(studentlist.ToList());
            }
            catch
            {
                return View(studentviewmodellist);
            }
        }

        public ActionResult StudentHistory(Guid id)
        {
            var studentlist = Service.StudentService.GetStudentById(id);
            if (studentlist == null)
            {
                StudentViewModel studentviewmodel = new StudentViewModel();
                return View(studentviewmodel);
            }

            return View(studentlist);
        }

        public ActionResult StudentUpdate(Guid id)
        {
            ViewBag.city_id = new SelectList(Service.DataService.GetCityList(), "city_id", "city_name");
            ViewBag.state_id = new SelectList(Service.DataService.GetStateList(), "state_id", "state_name");
            ViewBag.university_id = new SelectList(Service.UniversityService.GetUniversityList(), "UniversityId", "UniversityName");
            ViewBag.college_id = new SelectList(Service.CollegeService.GetCollegeList(), "college_id", "college_name");
            ViewBag.sem_id = new SelectList(Service.DataService.GetSemesterList(), "sem_id", "sem_no");
            ViewBag.department_id = new SelectList(Service.DepartmentService.GetDepartmentList(), "department_id", "department_name");
            ViewBag.role_list = new SelectList(Service.RoleService.GetRoleList(), "role_id", "role_name");
            var studentobj = Service.StudentService.GetStudentById(id);
            return View(studentobj);
        }

        [HttpPost]
        public String StudentUpdate(StudentViewModel studentModel)
        {
            bool result = Service.StudentService.UpdateStudent(studentModel);
            if (result == true)
            {
                return "User Updated Successfully";
            }
            else
            {
                return "Error while Updating Student";
            }           

        }

        public ActionResult StudentDelete(Guid id)
        {
            var studentlist = Service.StudentService.GetStudentById(id);
            if (studentlist == null)
            {
                StudentViewModel studentviewmodel = new StudentViewModel();
                return View(studentviewmodel);
            }

            return View(studentlist);
        }
        [HttpPost]
        public String StudentDelete(StudentViewModel studentviewmodel)
        {
            bool result = Service.StudentService.DeleteStudent(studentviewmodel.student_id);
            if (result == true)
            {
                return "Student Deleted";
            }
            else
            {
                return "Student Deletion Failed";
            }

                
           
        }

        public ActionResult CollegeDepartmentPortal()
        {
            ViewBag.CollegeList = new SelectList(Service.CollegeService.GetCollegeList(), "college_id", "college_name");
            ViewBag.DepartmentList = new SelectList(Service.DepartmentService.GetDepartmentList(), "department_id", "department_name");
            return View();
        }
        public ActionResult CollegeDepartmentPartialList(int? college_id)
        {
            if (college_id.HasValue)
            {
                var CollegeDepartmentList = Service.CollegeService.GetCollegeByID((int)college_id);
                return PartialView("_CollegeDepartment", CollegeDepartmentList);

            }
            CollegeViewModel collegeViewModel = new CollegeViewModel();


            return PartialView("_CollegeDepartment", collegeViewModel);
        }
        [HttpPost]
        public String CollegeDepartmentPortal(CollegeDepartmentHelperPortal collegeDepartmentHelperPortal)
        {
            bool result = Service.CollegeService.AddDepartment(collegeDepartmentHelperPortal);
            if (result == true) { return "Deparment Added"; } else { return "Department already existed"; }
        }


        public String CollegeDepartmentDelete(int? clg_id, int? dep_id)
        {
            if (clg_id.HasValue && dep_id.HasValue)
            {

                bool result = Service.CollegeService.DeleteDepartment(clg_id.Value, dep_id.Value);
                if (result == true) { return "Deparment Deleted"; } else { return "Department Deletion Failed"; }
            }
            return "Deletion Failed";

        }

        public ActionResult AddCollege()
        {
            ViewBag.city_id = new SelectList(Service.DataService.GetCityList(), "city_id", "city_name");
            ViewBag.state_id = new SelectList(Service.DataService.GetStateList(), "state_id", "state_name");
            ViewBag.unilist = new SelectList(Service.UniversityService.GetUniversityList(), "UniversityId", "UniversityName");
            return View();
        }

        [HttpPost]
        public String AddCollege(CollegeViewModel collegemodel)
        {
            bool result = Service.CollegeService.AddCollege(collegemodel);
            if (result == true)
            {
                return "College added Successfully";
            }
            else
            {
                return "Error while Adding College";
            }

        }

        public ActionResult AddDepartment()
        {
            return View();
        }

        [HttpPost]
        public String AddDepartment(DepartmentViewModel departmentmodel)
        {
            bool result = Service.DepartmentService.AddDepartment(departmentmodel);
            if (result == true)
            {
                return "Department Added Successfully";
            }
            else
            {
                return "Error while Adding Department";
            }

        }

        public ActionResult CollegePartialList()
        {
            var university = Service.UniversityService.GetUniversityDetails();


            return PartialView("_CollegeList", university);
        }
        public ActionResult DepartmentPartialList()
        {
            var department = Service.DepartmentService.GetDepartmentList();


            return PartialView("_DepartmentList", department);
        }
        public ActionResult UpdateCollege(int id)
        {
            CollegeViewModel clgmodel = new CollegeViewModel();
            clgmodel = Service.CollegeService.GetCollegeByID(id);
            ViewBag.citylist = new SelectList(Service.DataService.GetCityList(), "city_id", "city_name");
            ViewBag.statelist = new SelectList(Service.DataService.GetStateList(), "state_id", "state_name");
            ViewBag.unilist = new SelectList(Service.UniversityService.GetUniversityList(), "UniversityId", "UniversityName");
            return View(clgmodel);
        }

        [HttpPost]
        public String UpdateCollege(CollegeViewModel collegeviewmodel)
        {
            bool result = Service.CollegeService.UpdateCollege(collegeviewmodel);
            if (result == true)
            {
                return "Department Added Successfully";
            }
            else
            {
                return "Error while Adding Department";
            }

        }

        public String DeleteCollege(int? clg_id)
        {
            if (clg_id.HasValue)
            {

                bool result = Service.CollegeService.DeleteCollege((int)clg_id);
                if (result == true) { return "College Deleted"; } else { return "College Deletion Failed"; }
            }
            return "Deletion Failed";

        }

        public String DeleteDepartment(int? dep_id)
        {
            if (dep_id.HasValue)
            {

                bool result = Service.DepartmentService.DeleteDepartment((int)dep_id);
                if (result == true) { return "Department Deleted"; } else { return "Department Deletion Failed"; }
            }
            return "Deletion Failed";

        }
        public ActionResult UpdateDepartment(int id)
        {
            DepartmentViewModel depmodel = new DepartmentViewModel();
            try
            {
                depmodel = Service.DepartmentService.GetDepartmentById(id);
                return View(depmodel);
            }
            catch (Exception e) { return View(depmodel); }

        }

        [HttpPost]
        public String UpdateDepartment(DepartmentViewModel departmentviewmodel)
        {
            bool result = Service.DepartmentService.UpdateDepartment(departmentviewmodel);
            if (result == true)
            {
                return "Department Updated Successfully";
            }
            else
            {
                return "Error while Updating Department";
            }

        }

        

        


    }
}

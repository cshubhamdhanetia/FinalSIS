﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLayer;
using EntitesViewModel.ViewModels;
using StudentInformationSystem.App_Start;

namespace StudentInformationSystem.Controllers
{
    [HandleError]
    [SessionExpire]
    [Authorize(Roles ="Student")]
    public class StudentController : Controller
    {
       
        
        public ActionResult Index()
        {
            
                StudentViewModel studentViewModel = new StudentViewModel();
                var email = Convert.ToString(Session["Email"]);
                studentViewModel = Service.StudentService.GetStudentByEmail(email);
                if (studentViewModel != null)
                {
                    return View(studentViewModel);
                }
                throw new Exception();
           
        }
        public ActionResult ViewHistory()
        {
            var email = Convert.ToString(Session["email"]);
            var student = Service.StudentService.GetStudentByEmail(email);           
            return View(student);
        }
        public ActionResult ChangePassword()
        {
            if (ModelState.IsValid)
            {
                return View();

            }
            return View();
        }
                        
        [HttpPost]
        public String ChangePassword(ChangePasswordViewModel changemodel)
        {
            bool result;
            result = Service.AuthenticationService.UpdatePassword(changemodel);
            if (result == true)
                return "Password updated successfully";
            else
                return "Password updation failed";
            
        }

    }
}
